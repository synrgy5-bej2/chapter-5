package org.binar.chapter5.service;

import lombok.extern.slf4j.Slf4j;
import org.binar.chapter5.model.Mahasiswa;
import org.binar.chapter5.repository.MahasiswaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Slf4j
@Service
public class MahasiswaService implements IMahasiswaService{

    @Autowired
    MahasiswaRepository mahasiswaRepository;

    public MahasiswaService() {

    }

    public MahasiswaService(MahasiswaRepository mahasiswaRepository) {
        this.mahasiswaRepository = mahasiswaRepository;
    }

    @Override
    public void newMahasiswa(Mahasiswa mahasiswa) throws Exception {
//        Mahasiswa[] maha = new Mahasiswa[10];
//        List<Mahasiswa> mahasiswaList = Arrays.asList(maha);
//        mahasiswaRepository.saveAll(mahasiswaList);
        // validasi dulu mahasiswa nya ada atau engga di sistem induk atau mahasiswa sudah terdaftar
        List<Mahasiswa> mhs = mahasiswaRepository.findMahasiswaByNama(mahasiswa.getNama());
        if(mhs.size() > 0) {
            throw new Exception("Mahasiswa sudah terdaftar sebelumnya!");
        }

        // ...
        mahasiswaRepository.save(mahasiswa);
    }

    @Override
    public Mahasiswa searchMahasiswa(String nama) {
        return mahasiswaRepository.findMahasiswaByNama(nama).get(0);
    }

    @Override
    public Mahasiswa searchMahasiswaWithAngkatan(String nama, Integer angkatan) {
        return mahasiswaRepository.findMahasiswaByNamaAndAngkatan(nama, angkatan).get(0);
    }

    @Override
    public List<Mahasiswa> mahasiswaPagination(Pageable pageable) {
        return mahasiswaRepository.findAllMahasiswa(pageable);
    }

    public List<Mahasiswa> mahasiswaFiltering(String[] filter, String[] data) {
        // Buat sql dasar nya
        String sql = "SELECT * FROM mahasiswa ";
        // Masukan filter opsional nya
        if(filter.length > 0) {
            sql += "WHERE " + filter[0] + " = " + data[0];
            for(int i = 1; i < filter.length; i++) {
                sql += " AND " + filter[i] + " = " + data[i];
            }
        }
        log.info("Generated SQL " + sql);

        // Persiapan preparedStatement dengan try with resources
        try(Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/chapter_4", "postgres", "password");
                PreparedStatement ps = conn.prepareStatement(sql)) {
            List<Mahasiswa> mahasiswaList = new ArrayList<>();
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                Mahasiswa mhs = Mahasiswa.builder()
                        .idMahasiswa(rs.getInt("id"))
                        .nama(rs.getString("nama"))
                        .build();
                mahasiswaList.add(mhs);
            }
            return mahasiswaList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }
}
