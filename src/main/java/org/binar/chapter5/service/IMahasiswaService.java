package org.binar.chapter5.service;

import org.binar.chapter5.model.Mahasiswa;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IMahasiswaService {

    void newMahasiswa(Mahasiswa mahasiswa) throws Exception;
    Mahasiswa searchMahasiswa(String nama);
    Mahasiswa searchMahasiswaWithAngkatan(String nama, Integer angkatan);
    List<Mahasiswa> mahasiswaPagination(Pageable pageable);

}
