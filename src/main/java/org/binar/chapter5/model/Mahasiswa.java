package org.binar.chapter5.model;

import lombok.*;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Entity
public class Mahasiswa {

    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idMahasiswa;

    private String nama;

    private String jurusan;

    private Integer angkatan;

    private String kodeJurusan;

    @ManyToOne
    private Kelas kelas;
}
