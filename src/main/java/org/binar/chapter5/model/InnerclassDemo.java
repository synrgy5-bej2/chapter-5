package org.binar.chapter5.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class InnerclassDemo {

    @Id
    private String id;

    private List<Tipe> tipe;
    private Durasi durasi;
    private Harga harga;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Tipe {
        private boolean putra;
        private boolean putri;
        private boolean campuran;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Durasi {
        private boolean harian;
        private boolean mingguan;
        private boolean bulanan;
        private boolean tigaBulanan;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Harga {
        private Long minimal;
        private Long maksimal;
    }
}
